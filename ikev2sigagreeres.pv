free Chan: channel.
free ChanReveal: channel.

(* Agents *)
const A, B, C: bitstring.

(* Active/Passive adversary *)
set attacker = active.

(*
 * Shared key encryption
 *)

fun enc(bitstring, bitstring): bitstring.
reduc forall x: bitstring, y: bitstring; dec(enc(x, y), y) = x.

(*
 * MAC with message recovery
 *)

fun rmac(bitstring, bitstring): bitstring.
reduc forall m: bitstring, k: bitstring; getmess(rmac(m, k)) = m.
reduc forall m: bitstring, k: bitstring; checkrmac(rmac(m, k), k) = m.

(*
 * Digital signature
 *)

fun prk(bitstring): bitstring [private]. (* Get private key of an agent *)
fun pbk(bitstring): bitstring. (* Get public key of an agent *)
fun sign(bitstring, bitstring): bitstring.

(* "m" represents a message, "k" represents a key,
and "a" represents an agent. *)
reduc forall m: bitstring, a: bitstring;
    checksign(sign(m, prk(a)), pbk(a), m) = true.

(*
 * Diffie-Hellman
 *)

(* The generator (often equals 2 in implementations) *)
const G: bitstring.

(* exp function represents modular exponentiation function *)
fun exp(bitstring, bitstring): bitstring.

(* g^(ab) = g^(ba) *)
equation forall x: bitstring, y: bitstring;
    exp(exp(G, x), y) = exp(exp(G, y), x).

(*
 * Secrecy terms and queries
 *)

(* Secret sent by Initiator at the end of the protocol *)
free SecretIni: bitstring [private].
(* Secret sent by Responder at the end of the protocol *)
free SecretRes: bitstring [private].

(* Query whether secrets are secret *)
query attacker(SecretIni); attacker(SecretRes).

(*
 * Authentication terms and queries
 *)

event iniRunning(bitstring, bitstring, bitstring).
event iniCommit(bitstring, bitstring, bitstring).
event resRunning(bitstring, bitstring, bitstring).
event resCommit(bitstring, bitstring, bitstring).

(* Injective agreement on keymat for Responder *)

query b: bitstring, k: bitstring;
inj-event(resCommit(b, A, k)) ==> inj-event(iniRunning(b, A, k)).

(*
 * Roles
 *)

(* Represents i's actions in a IKEv2-Sig session between an initiator i and a
responder r *)
let Initiator(i: bitstring, r: bitstring) =

    (* Init exchange *)
    new xi: bitstring;
    new ni: bitstring;
    let kei = exp(G, xi) in
    out(Chan, (false, kei, ni));
    in(Chan, (=true, ker: bitstring, nr: bitstring));
    let k = exp(ker, xi) in

    (* Auth exchange *)
    let authi = sign((k, i, kei, ni, nr), prk(i)) in
    let unmacedauthrequest = (false, enc((i, authi), k)) in

    event iniRunning(i, r, k);

    out(Chan, rmac(unmacedauthrequest, k));
    in(Chan, authresponse: bitstring);
    (* We check the MAC *)
    let unmacedauthresponse = checkrmac(authresponse, k) in
    (* We check the header and get the SDU (Service Data Unit) *)
    let (=true, authresponsesdu: bitstring) = unmacedauthresponse in
    (* We decrypt the SDU, check r and get authr *)
    let (=r, authr: bitstring) = dec(authresponsesdu, k) in
    (* We check authr *)
    if checksign(authr, pbk(r), (k, r, ker, nr, ni)) then (

    if (i = A && r = B) then (
    let keymat = k in
    event iniCommit(i, r, keymat);
    out(Chan, enc(SecretIni, keymat))
    ))
    .

let Initiatorwithoutinirunning(i: bitstring, r: bitstring) =

    (* Init exchange *)
    new xi: bitstring;
    new ni: bitstring;
    let kei = exp(G, xi) in
    out(Chan, (false, kei, ni));
    in(Chan, (=true, ker: bitstring, nr: bitstring));
    let k = exp(ker, xi) in

    (* Auth exchange *)
    let authi = sign((k, i, kei, ni, nr), prk(i)) in
    let unmacedauthrequest = (false, enc((i, authi), k)) in

    (* event iniRunning(i, r, k); *)

    out(Chan, rmac(unmacedauthrequest, k));
    in(Chan, authresponse: bitstring);
    (* We check the MAC *)
    let unmacedauthresponse = checkrmac(authresponse, k) in
    (* We check the header and get the SDU (Service Data Unit) *)
    let (=true, authresponsesdu: bitstring) = unmacedauthresponse in
    (* We decrypt the SDU, check r and get authr *)
    let (=r, authr: bitstring) = dec(authresponsesdu, k) in
    (* We check authr *)
    if checksign(authr, pbk(r), (k, r, ker, nr, ni)) then (

    if (i = A && r = B) then (
    let keymat = k in
    event iniCommit(i, r, keymat);
    out(Chan, enc(SecretIni, keymat))
    ))
    .

(* Represents r's actions in a IKEv2-Sig session between an initiator i and a
responder r *)
let Responder(r: bitstring) =

    (* Init exchange *)
    new xr: bitstring;
    new nr: bitstring;
    in(Chan, (=false, kei: bitstring, ni: bitstring));
    let ker = exp(G, xr) in
    out(Chan, (true, ker, nr));
    let k = exp(kei, xr) in

    (* Auth exchange *)
    in(Chan, authrequest: bitstring);
    (* We check the MAC *)
    let unmacedauthrequest = checkrmac(authrequest, k) in
    (* We check the header and get the SDU (Service Data Unit) *)
    let (=false, authrequestsdu: bitstring) = unmacedauthrequest in
    let (i: bitstring, authi: bitstring) = dec(authrequestsdu, k) in
    if checksign(authi, pbk(i), (k, i, kei, ni, nr)) then (
    let authr = sign((k, r, ker, nr, ni), prk(r)) in
    let unmacedauthresponse = (true, enc((r, authr), k)) in

    let keymat = k in
    event resRunning(i, r, keymat);

    out(Chan, rmac(unmacedauthresponse, k));

    if (i = B && r = A) then (
    event resCommit(i, r, k);
    out(Chan, enc(SecretRes, keymat))
    ))
    .

let Responderwithoutrescommit(r: bitstring) =

    (* Init exchange *)
    new xr: bitstring;
    new nr: bitstring;
    in(Chan, (=false, kei: bitstring, ni: bitstring));
    let ker = exp(G, xr) in
    out(Chan, (true, ker, nr));
    let k = exp(kei, xr) in

    (* Auth exchange *)
    in(Chan, authrequest: bitstring);
    (* We check the MAC *)
    let unmacedauthrequest = checkrmac(authrequest, k) in
    (* We check the header and get the SDU (Service Data Unit) *)
    let (=false, authrequestsdu: bitstring) = unmacedauthrequest in
    let (i: bitstring, authi: bitstring) = dec(authrequestsdu, k) in
    if checksign(authi, pbk(i), (k, i, kei, ni, nr)) then (
    let authr = sign((k, r, ker, nr, ni), prk(r)) in
    let unmacedauthresponse = (true, enc((r, authr), k)) in

    let keymat = k in
    event resRunning(i, r, keymat);

    out(Chan, rmac(unmacedauthresponse, k));

    if (i = B && r = A) then (
    (* event resCommit(i, r, k); *)
    out(Chan, enc(SecretRes, keymat))
    ))
    .

(*
 * Process
 *)

process
    
    (* We are in AdvINT *)
    out(ChanReveal, prk(C));

    (* Two honest and one dishonest agents, without self communication. We prove *)
    (* properties for A *)

    !Initiatorwithoutinirunning(A, B) |
    !Initiatorwithoutinirunning(A, C) |
    !Initiator(B, A) |
    !Initiatorwithoutinirunning(B, C) |
    !Responder(A) |
    !Responderwithoutrescommit(B) |

    0
