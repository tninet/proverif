free Chan: channel.
free ChanReveal: channel.

(* Agents *)
const A, B, C: bitstring.

(* Active/Passive adversary *)
set attacker = active.

(*
 * Shared key encryption
 *)

fun enc(bitstring, bitstring): bitstring.
reduc forall x: bitstring, y: bitstring; dec(enc(x, y), y) = x.

(*
 * Pre-shared key
 *)

(*
 * psk(x, y, xisinitiator) represents the key computed by x in the phase 1
 * session between x and y, where i was initiator if xisinitiator is true, or
 * where x was responder if xisinitiator is false.
 *)

fun psk(bitstring, bitstring, bool): bitstring [private].
equation forall x: bitstring, y: bitstring; psk(x, y, true) = psk(y, x, false).
equation forall x: bitstring, y: bitstring; psk(x, y, false) = psk(y, x, true).

(*
 * Message Authentication Code
 *)

fun mac(bitstring, bitstring): bitstring.
reduc forall m: bitstring, k: bitstring; getmess(mac(m, k)) = m.
reduc forall m: bitstring, k: bitstring; checkmac(mac(m, k), k) = m.

(*
 * Diffie-Hellman
 *)

(* The generator (often equals 2 in implementations) *)
const G: bitstring.

(* exp function represents modular exponentiation function *)
fun exp(bitstring, bitstring): bitstring.

(* g^(ab) = g^(ba) *)
equation forall x: bitstring, y: bitstring; exp(exp(G, x), y) = exp(exp(G, y),
x).

(*
 * Secrecy terms and queries
 *)

(* Secret sent by Initiator at the end of the protocol *)
free SecretIni: bitstring [private].
(* Secret sent by Responder at the end of the protocol *)
free SecretRes: bitstring [private].

(* Query whether secrets are secret *)
query attacker(SecretIni); attacker(SecretRes).

(*
 * Authentication terms and queries
 *)

event iniRunning(bitstring, bitstring, bitstring).
event iniCommit(bitstring, bitstring, bitstring).
event resRunning(bitstring, bitstring, bitstring).
event resCommit(bitstring, bitstring, bitstring).

(* Agreement on keymat for Responder *)

query b: bitstring, k: bitstring;
inj-event(resCommit(b, A, k)) ==> inj-event(iniRunning(b, A, k)).

(*
 * Roles
 *)

(*
 * Represents i's actions in a IKEv2-Child session between an initiator i and a
 * responder r. origini is true when i is the original IKEv2 initiator, and
 * false otherwise.
 *)

let Initiator(i: bitstring, r: bitstring, origini: bool) =

    (* Child exchange *)
    new xi: bitstring;
    new ni: bitstring;
    let kei = exp(G, xi) in
    let k = psk(i, r, origini) in
    let unmacedrequest = (false, origini, enc((kei, ni), k)) in
    event iniRunning(i, r, kei);
    out (Chan, mac(unmacedrequest, k));
    in (Chan, response: bitstring);
    let unmacedresponse = checkmac(response, k) in
    let (=true, =not(origini), responsesdu: bitstring) = unmacedresponse in
    let (ker: bitstring, nr: bitstring) = dec(responsesdu, k) in
    let keymat = exp(ker, xi) in
    if (i = A && r = B) then (
    event iniCommit(i, r, keymat);
    out (Chan, enc(SecretIni, keymat))
    )
    .

let Initiatorwithoutinirunning(i: bitstring, r: bitstring, origini: bool) =

    (* Child exchange *)
    new xi: bitstring;
    new ni: bitstring;
    let kei = exp(G, xi) in
    let k = psk(i, r, origini) in
    let unmacedrequest = (false, origini, enc((kei, ni), k)) in
    (* event iniRunning(i, r, kei); *)
    out (Chan, mac(unmacedrequest, k));
    in (Chan, response: bitstring);
    let unmacedresponse = checkmac(response, k) in
    let (=true, =not(origini), responsesdu: bitstring) = unmacedresponse in
    let (ker: bitstring, nr: bitstring) = dec(responsesdu, k) in
    let keymat = exp(ker, xi) in
    if (i = A && r = B) then (
    event iniCommit(i, r, keymat);
    out (Chan, enc(SecretIni, keymat))
    )
    .

(*
 * Represents r's actions in a IKEv2-Child session between an initiator i and a
 * responder r. origini is true when i is the original IKEv2 initiator, and
 * false otherwise.
 *)

let Responder(i: bitstring, r: bitstring, origini: bool) =

    (* Child exchange *)
    new xr: bitstring;
    new nr: bitstring;
    let ker = exp(G, xr) in
    let k = psk(r, i, not(origini)) in
    in (Chan, request: bitstring);
    let unmacedrequest = checkmac(request, k) in
    let (=false, =origini, requestsdu: bitstring) = unmacedrequest in
    let (kei: bitstring, ni: bitstring) = dec(requestsdu, k) in
    let unmacedresponse = (true, not(origini), enc((ker, nr), k)) in
    let keymat = exp(kei, xr) in
    event resRunning(i, r, keymat);
    out (Chan, mac(unmacedresponse, k));
    if (i = B && r = A) then (
    event resCommit(i, r, kei);
    out (Chan, enc(SecretRes, keymat))
    )
    .

let Responderwithoutrescommit(i: bitstring, r: bitstring, origini: bool) =

    (* Child exchange *)
    new xr: bitstring;
    new nr: bitstring;
    let ker = exp(G, xr) in
    let k = psk(r, i, not(origini)) in
    in (Chan, request: bitstring);
    let unmacedrequest = checkmac(request, k) in
    let (=false, =origini, requestsdu: bitstring) = unmacedrequest in
    let (kei: bitstring, ni: bitstring) = dec(requestsdu, k) in
    let unmacedresponse = (true, not(origini), enc((ker, nr), k)) in
    let keymat = exp(kei, xr) in
    event resRunning(i, r, keymat);
    out (Chan, mac(unmacedresponse, k));
    if (i = B && r = A) then (
    (* event resCommit(i, r, kei); *)
    out (Chan, enc(SecretRes, keymat))
    )
    .

(*
 * Main process
 *)

process
    
    (* We are in AdvINT *)
    out (ChanReveal, psk(A, C, true));
    out (ChanReveal, psk(B, C, true));
    out (ChanReveal, psk(A, C, false));
    out (ChanReveal, psk(B, C, false));

    !Initiatorwithoutinirunning ( A, B, true  ) |
    !Initiatorwithoutinirunning ( A, C, true  ) |
    !Initiator                  ( B, A, true  ) |
    !Initiatorwithoutinirunning ( B, C, true  ) |
    !Responderwithoutrescommit  ( A, B, true  ) |
    !Responderwithoutrescommit  ( C, B, true  ) |
    !Responder                  ( B, A, true  ) |
    !Responderwithoutrescommit  ( C, A, true  ) |
    !Initiatorwithoutinirunning ( A, B, false ) |
    !Initiatorwithoutinirunning ( A, C, false ) |
    !Initiatorwithoutinirunning ( B, A, false ) |
    !Initiatorwithoutinirunning ( B, C, false ) |
    !Responderwithoutrescommit  ( A, B, false ) |
    !Responderwithoutrescommit  ( C, B, false ) |
    !Responderwithoutrescommit  ( B, A, false ) |
    !Responderwithoutrescommit  ( C, A, false ) |

    0
